﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.SignalRHub
{
    public class CustomerHub: Hub<IGreeterHubClient>
    {
        private readonly ILogger<CustomerHub> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerHub(ILogger<CustomerHub> logger, IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<IEnumerable<Customer>> GetAllCustomers()
        {
            var customers =  await _customerRepository.GetAllAsync();

            return customers;

        }

        public async Task<Customer> GetCustomerById(Guid id)
        {            
            var customer =  await _customerRepository.GetByIdAsync(id);

            return customer;

        }

        public async Task<bool> CreateCustomer(Customer customer)
        {
            await _customerRepository.AddAsync(customer);

            return true;
        }
        public override async Task OnConnectedAsync()
        {
            _logger.LogInformation($"{Context.ConnectionId} connected");

            await base.OnConnectedAsync();
        }
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            _logger.LogInformation($"{Context.ConnectionId} disconnected");

            await base.OnDisconnectedAsync(exception);
        }
    }
}


using System;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.grpcService;

namespace Otus.Teaching.PromoCodeFactory.gRpcService.Services
{
    public class CustomerGrpcService : CustomerGrpcServiceProto.CustomerGrpcServiceProtoBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerGrpcService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        public override async Task<CustomerMessage> GetCustomerById(IdMessage idMsg, ServerCallContext context)
        {
            var id = Guid.Parse(idMsg.Id);
            
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerMessage()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            return response;
        }

        public override async Task<CustomersMessage> GetAllCustomers(Empty empty, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersMsg = new CustomersMessage();

            foreach (var customer in customers)
            {
                var customerMsg = new CustomerMessage
                {
                    Id = customer.Id.ToString(),
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                };

                customersMsg.Customers.Add(customerMsg);
            }
            return customersMsg;
        }


        public override async Task<Empty> CreateCustomer(CustomerMessage customerMsg, ServerCallContext context)
        {
            var customer = new Customer
            {
                Id = Guid.Parse(customerMsg.Id),
                FirstName = customerMsg.FirstName,
                LastName = customerMsg.LastName,
                Email = customerMsg.Email
            };

            await _customerRepository.AddAsync(customer);

            return new Empty();
        }
    }

}
﻿using System;
using Microsoft.AspNetCore.SignalR.Client;

namespace Otus.Teaching.PromoCodeFactory.SignalrClient
{
    public class SignalrConnectionFactory: ISignalrConnectionFactory
    {
        private readonly string _url;

        public SignalrConnectionFactory(string url)
        {
            _url = url;
        }
        public HubConnection GetHubConnection()
        {
            return  new HubConnectionBuilder()
                .WithUrl(_url)
                .WithAutomaticReconnect(new TimeSpan[10])
                .Build();
        }
    }
}
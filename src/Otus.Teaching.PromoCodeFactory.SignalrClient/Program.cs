﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.SignalrClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await Task.Delay(5000);
            
            var connection = new SignalrConnectionFactory("http://localhost:5001/customers").GetHubConnection();

            await connection.StartAsync();

            var inProcess = true;

            while (inProcess)
            {
                Console.WriteLine("Welcome to signalR tester. Choose command: ");
                Console.WriteLine("1 - get all customers");
                Console.WriteLine("2 - get once customer");
                Console.WriteLine("3 - create test customer");
                Console.WriteLine("0 - exit");

                string key = Console.ReadLine();

                switch (key)
                {
                    case "0":
                        await StopConnection(connection);
                        inProcess = false;
                        break;
                    case "1":
                        await ShowAllCustomers(connection);
                        break;
                    case "2":
                        await ShowCustomer(connection);
                        break;
                    case "3":
                        await CreateateCustomer(connection);
                        break;
                    default:
                        Console.WriteLine("you've sent a unknown command. Pls try again");
                        break;
                }
            }

            Console.ReadLine();
        }

        private static async Task CreateateCustomer(HubConnection connection)
        {
            var testCustomer = FakeDataFactory.Customers.First();

            testCustomer.Id = Guid.NewGuid();

            var result = await connection.InvokeAsync<bool>("CreateCustomer", testCustomer);

            Console.WriteLine(result);
        }

        public static async Task StopConnection(HubConnection connection)
        {
            await connection.StopAsync();
        }

        public static async Task ShowAllCustomers(HubConnection connection)
        {
            var customers = await connection.InvokeAsync<IEnumerable<Customer>>("GetAllCustomers");

            foreach (var customer in customers)
            {
                Console.WriteLine(JsonSerializer.Serialize(customer)); 
            }
        }

        public static async Task ShowCustomer(HubConnection connection)
        {
           var id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0";

           var customer = await connection.InvokeAsync<Customer>("GetCustomerById", id);

           Console.WriteLine(JsonSerializer.Serialize(customer));

        }
}
}

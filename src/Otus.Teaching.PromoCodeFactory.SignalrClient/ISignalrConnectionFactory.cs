﻿using Microsoft.AspNetCore.SignalR.Client;

namespace Otus.Teaching.PromoCodeFactory.SignalrClient
{
    public interface ISignalrConnectionFactory
    {
        HubConnection GetHubConnection();
    }
}
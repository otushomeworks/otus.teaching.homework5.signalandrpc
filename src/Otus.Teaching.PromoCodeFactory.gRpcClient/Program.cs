﻿using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.grpcService;


namespace Otus.Teaching.PromoCodeFactory.gRpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await Task.Delay(5000);

            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            using var connection = GrpcChannel.ForAddress("http://localhost:5050");
            var grpcClient = new CustomerGrpcServiceProto.CustomerGrpcServiceProtoClient(connection); 

            var inProcess = true;

            while (inProcess)
            {
                Console.WriteLine("Welcome to signalR tester. Choose command: ");
                Console.WriteLine("1 - get all customers");
                Console.WriteLine("2 - get once customer");
                Console.WriteLine("3 - create test customer");
                Console.WriteLine("0 - exit");

                string key = Console.ReadLine();

                switch (key)
                {
                    case "0":
                        inProcess = false;
                        break;
                    case "1":
                        await ShowAllCustomers(grpcClient);
                        break;
                    case "2":
                        await ShowCustomer(grpcClient);
                        break;
                    case "3":
                        await CreateateCustomer(grpcClient);
                        break;
                    default:
                        Console.WriteLine("you've sent a unknown command. Pls try again");
                        break;
                }
            }
        }

        private static async Task CreateateCustomer(CustomerGrpcServiceProto.CustomerGrpcServiceProtoClient grpcClient)
        {
            var testCustomer = FakeDataFactory.Customers.First();

            testCustomer.Id = Guid.NewGuid();

            await grpcClient.CreateCustomerAsync(new CustomerMessage
            {
                Id = testCustomer.Id.ToString(),
                FirstName = testCustomer.FirstName,
                Email = testCustomer.Email,
                LastName = testCustomer.LastName
            });

            Console.WriteLine("success");
        }

        public static async Task ShowAllCustomers(CustomerGrpcServiceProto.CustomerGrpcServiceProtoClient grpcClient)
        {
            var customers = await grpcClient.GetAllCustomersAsync(new Google.Protobuf.WellKnownTypes.Empty());

            foreach (var customer in customers.Customers)
            {
                Console.WriteLine(JsonSerializer.Serialize(customer)); 
            }
        }

        public static async Task ShowCustomer(CustomerGrpcServiceProto.CustomerGrpcServiceProtoClient grpcClient)
        {
            var id = new IdMessage
            {
                Id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0"
            };

            var customer = await grpcClient.GetCustomerByIdAsync(id);

            Console.WriteLine(JsonSerializer.Serialize(customer));
        }
    }
}
